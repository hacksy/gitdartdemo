
import 'package:git_client/commands/init.dart';

void main(List<String> arguments) {
  if(arguments.isNotEmpty){
    if(arguments[0] == 'init'){
        var dir = arguments.length> 1 ? arguments[1] : '';
        GitInit().call(InitParameters(baseRoute: dir));
    }

     if(arguments[0] == 'add'){
        GitInit().call(InitParameters.fromArguments(arguments.sublist(1)));
    }
  }
}




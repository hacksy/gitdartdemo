abstract class Command<Params>{
  void call(Params params);
}


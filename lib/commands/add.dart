import 'package:git_client/core/command.dart';
/*
git add [--verbose | -v] [--dry-run | -n] [--force | -f] [--interactive | -i] [--patch | -p]
	  [--edit | -e] [--[no-]all | --[no-]ignore-removal | [--update | -u]]
	  [--intent-to-add | -N] [--refresh] [--ignore-errors] [--ignore-missing] [--renormalize]
	  [--chmod=(+|-)x] [--pathspec-from-file=<file> [--pathspec-file-nul]]
	  [--] [<pathspec>…​]
    */
class GitAdd extends Command<AddCommands>{
  @override
  void call(AddCommands params) {
    
  }
}

class AddCommands {
  AddCommands();
}
/*
 init [-q | --quiet] [--bare] [--template=<template_directory>]
	  [--separate-git-dir <git dir>] [--object-format=<format>]
	  [-b <branch-name> | --initial-branch=<branch-name>]
	  [--shared[=<permissions>]] [directory]
*/

import 'package:git_client/core/command.dart';
 
class GitInit implements Command<InitParameters>{
  @override
  void call(InitParameters params) {
    print(params);
    //Create directories and files
  }
}

class InitParameters{
    final String baseRoute;
    final bool isBare;
    final bool isQuiet;
    InitParameters({this.baseRoute, this.isBare = false, this.isQuiet = false});
    factory InitParameters.fromArguments(List<String> arguments){
      print(arguments);
      return InitParameters();
    }
    @override
    String toString(){
      return 'InitParameters({baseRoute: $baseRoute, isBare: $isBare})';
    }
}